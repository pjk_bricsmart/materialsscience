#
from mdf_forge import Forge
mdf = Forge()
mdf.match_source_names("narayananbadri_g4mp2gdb9_database")
mdf.search()

import ase.db

# Connect to the ASE database
db = ase.db.connect("g4mp2-gdb9.db")

# Find a molecule using its InChI representation
mol = db.get(smiles = 'CC')
